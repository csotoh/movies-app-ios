//
//  DetailMovieInteractor.swift
//  Peliculas
//
//  Created by Soto Rodriguez on 1/12/21.
//

import Foundation

class DetailMovieInteractor: DetailMovieInteractorContract {
    
    var callback: DetailMovieCallbackContract?
    var manager = DetailMovieManager()
    
    //MARK: Interactor
    func getDetailMovie(id: Int) {
        self.manager.getDetailMovie(id: id, completion: { (detail, failure) in
            guard failure == nil else{
                self.callback?.error(failure: failure!)
                return
            }
            self.callback?.detailMovie(detail: MoviesMapper.parse(from: detail!))
        })
    }
    
    func setCallback(callback: DetailMovieCallbackContract) {
        self.callback = callback
    }
    
    
}
