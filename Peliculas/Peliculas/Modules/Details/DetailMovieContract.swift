//
//  DetailMovieContract.swift
//  Peliculas
//
//  Created by Soto Rodriguez on 1/12/21.
//

import Foundation

protocol DetailMovieViewContract{
    func showMessage(message: String)
    func updateDetailMovie(detailMovie: DetailMovieEntity)
    func noConnection()
    func presentLoading()
}

protocol DetailMoviePresenterContract{
    func detailMovie(id: Int)
}

protocol DetailMovieInteractorContract {
    func getDetailMovie(id: Int)
    func setCallback(callback: DetailMovieCallbackContract)
}

protocol DetailMovieCallbackContract {
    func error(failure: Failure)
    func detailMovie(detail: DetailMovieEntity)
}
