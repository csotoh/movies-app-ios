//
//  DetailMoviePresenter.swift
//  Peliculas
//
//  Created by Soto Rodriguez on 1/12/21.
//

import Foundation

class DetailMoviePresenter: DetailMoviePresenterContract, DetailMovieCallbackContract {
    
    var view: DetailMovieViewContract?
    var interactor: DetailMovieInteractorContract?
    
    init(view: DetailMovieViewContract){
        self.view = view
        self.interactor = DetailMovieInteractor()
        self.interactor?.setCallback(callback: self)
    }
    
    //MARK: Presenter
    func detailMovie(id: Int) {
        self.view?.presentLoading()
        self.interactor?.getDetailMovie(id: id)
    }
    
    //MARK: Callback
    func error(failure: Failure) {
        guard failure.code != CodeFailure.connection else {
            self.view?.noConnection()
            return
        }
        self.view?.showMessage(message: failure.getMessageFailure())
    }
    
    func detailMovie(detail: DetailMovieEntity) {
        self.view?.updateDetailMovie(detailMovie: detail)
    }
    
    
}
