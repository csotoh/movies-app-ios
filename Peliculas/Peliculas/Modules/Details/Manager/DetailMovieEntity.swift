//
//  DetailMovieEntity.swift
//  Peliculas
//
//  Created by Soto Rodriguez on 1/12/21.
//

import Foundation

class DetailMovieEntity {
    
    var id: Int
    var title: String
    var backdropPath: String
    var originalTitle: String
    var overview: String
    var voteAverange: Double
    var posterPath: String
    
    init(id: Int, title: String, backdropPath: String, originalTitle: String, overview: String, voteAverage: Double, posterPath: String) {
        self.id = id
        self.title = title
        self.originalTitle = originalTitle
        self.overview = overview
        self.voteAverange = voteAverage
        self.backdropPath = backdropPath == "" ? "http://www.musicapopular.cult.cu/wp-content/uploads/2017/12/imagen-no-disponible.png" : "https://image.tmdb.org/t/p/w500" + backdropPath
        self.posterPath = posterPath == "" ? "http://quimicaactiva.com/wp-content/uploads/2017/08/no-disponible.png" : "https://image.tmdb.org/t/p/w500" + posterPath
        
    }
}
