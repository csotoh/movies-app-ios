//
//  DetailMovieManager.swift
//  Peliculas
//
//  Created by Soto Rodriguez on 1/12/21.
//

import Foundation

class DetailMovieManager{
    
    var object: DetailMovieApiModel?
    
    func getDetailMovie(id: Int, completion: @escaping (DetailMovieApiModel?, Failure?) -> ()) {
        
        MoviesAPI.shared.doGetServices(service: MoviesAPI.Service.detail, parameters: ParameterGetDetail(id: id))
        MoviesAPI.shared.completionHandler{ (data, failure) in
            guard failure == nil else {
                completion(nil, failure)
                return
            }
            guard let response = data as? DetailMovieApiModel else{
                completion(nil, Failure(.read))
                return
            }
            completion(response, nil)
        }
        
    }
}
