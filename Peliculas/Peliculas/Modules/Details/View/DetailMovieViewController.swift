//
//  DetailMovieViewController.swift
//  Peliculas
//
//  Created by Soto Rodriguez on 1/12/21.
//

import UIKit

class DetailMovieViewController: UIViewController, DetailMovieViewContract {
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var stackNoConnection: UIStackView!
    @IBOutlet weak var stackLoading: UIStackView!
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var imageBackdrop: UIImageView!
    @IBOutlet weak var imagePoster: UIImageView!
    @IBOutlet weak var textTitle: UILabel!
    @IBOutlet weak var originalTit: UILabel!
    @IBOutlet weak var voteAverage: UILabel!
    @IBOutlet weak var overview: UILabel!
    
    var id: Int = 0
    var presenter: DetailMoviePresenterContract?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.presenter = DetailMoviePresenter(view: self)
        self.presenter?.detailMovie(id: self.id)
        
        self.imagePoster.layer.cornerRadius = 10
        self.imagePoster.clipsToBounds = true
    }
    
    
    //MARK: View
    func showMessage(message: String) {
        Utilities.showAlertWithOneCallback("Películas", msg: message, controller: self, callBackOk: {
            self.presenter?.detailMovie(id: self.id)
        })
    }
    
    func updateDetailMovie(detailMovie: DetailMovieEntity) {
        self.stackNoConnection.isHidden = true
        self.stackLoading.isHidden = true
        self.scrollView.isHidden = false
        self.navigationBar.topItem?.title = detailMovie.title
        self.imageBackdrop.downloaded(from: detailMovie.backdropPath)
        self.imagePoster.downloaded(from: detailMovie.posterPath)
        self.textTitle.text = detailMovie.title
        self.originalTit.text = detailMovie.originalTitle
        self.overview.text = detailMovie.overview
        self.voteAverage.text = "\(detailMovie.voteAverange)"
    }
    
    func noConnection() {
        self.stackLoading.isHidden = true
        self.stackNoConnection.isHidden = false
    }
    
    func presentLoading() {
        self.stackLoading.isHidden = false
        self.stackNoConnection.isHidden = true
    }

    //MARK: Acciones
    @IBAction func back(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func retry(_ sender: UIButton) {
        self.presenter?.detailMovie(id: self.id)
    }
}
