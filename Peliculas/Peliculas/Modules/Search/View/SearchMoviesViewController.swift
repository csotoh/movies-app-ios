//
//  SearchMoviesViewController.swift
//  Peliculas
//
//  Created by Soto Rodriguez on 2/12/21.
//

import UIKit

class SearchMoviesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, SearchMoviesViewContract {
    
    
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableSearch: UITableView!
    
    let cellIdentifier = "searchCell"
    var queryString = ""
    var listMovies: [SearchMovieEntity] = []
    var presenter: SearchMoviesPresenterContract?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableSearch.register(UINib(nibName: "SearchMovieViewCell", bundle: nil), forCellReuseIdentifier: cellIdentifier)
        self.tableSearch.delegate = self
        self.tableSearch.dataSource = self
        self.searchBar.delegate = self
        
        self.presenter = SearchMoviesPresenter(view: self)
    }
    
    //MARK: View
    func showMessage(message: String) {
        Utilities.showAlertWithOneCallback("Películas", msg: message, controller: self, callBackOk: {
            self.presenter?.searchMovie(query: self.queryString)
        })
    }
    
    func updateListMovies(list: [SearchMovieEntity]) {
        self.listMovies = list
        self.tableSearch.reloadData()
    }
    
    //MARK: TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listMovies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableSearch.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! SearchMovieViewCell
        
        cell.movie = self.listMovies[indexPath.row]
        cell.setUp()
        cell.dropShadow()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableSearch.deselectRow(at: indexPath, animated: false)
        let id = self.listMovies[indexPath.row].id
        let vc = DetailMovieViewController(nibName: "DetailMovieViewController", bundle: nil)
        vc.id = id
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true)
    }

    //MARK: SearchDelegate
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.queryString = searchText
        self.presenter?.searchMovie(query: searchText)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    //MARK: Acciones
    @IBAction func back(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
}
