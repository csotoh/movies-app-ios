//
//  SearchMovieViewCell.swift
//  Peliculas
//
//  Created by Soto Rodriguez on 2/12/21.
//

import UIKit

class SearchMovieViewCell: UITableViewCell {

    @IBOutlet weak var imagePoster: UIImageView!
    @IBOutlet weak var titleMovie: UILabel!
    @IBOutlet weak var originalTitle: UILabel!
    
    
    var movie: SearchMovieEntity?
    
    func setUp() {
        guard let movie = self.movie else { return }
        imagePoster.downloaded(from: movie.posterPath)
        imagePoster.layer.cornerRadius = 10
        imagePoster.clipsToBounds = true
        titleMovie.text = movie.title
        originalTitle.text = movie.originalTitle
    }
    
}
