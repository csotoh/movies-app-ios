//
//  SearchMoviesPresenter.swift
//  Peliculas
//
//  Created by Soto Rodriguez on 2/12/21.
//

import Foundation
import UIKit

class SearchMoviesPresenter: SearchMoviesPresenterContract, SearchMoviesCallbackContract {
    
    var view: SearchMoviesViewContract?
    var interactor: SearchMoviesInteractorContract?
    
    init(view: SearchMoviesViewContract){
        self.view = view
        self.interactor = SearchMoviesInteractor()
        self.interactor?.setCallback(callback: self)
    }
    
    //MARK: Presenter
    func searchMovie(query: String) {
        guard !query.isEmpty else { return }
        self.interactor?.searchMovies(query: query)
    }
    
    //MARK: Callback
    func error(failure: Failure) {
        self.view?.showMessage(message: failure.getMessageFailure())
    }
    
    func listMovies(list: [SearchMovieEntity]) {
        self.view?.updateListMovies(list: list)
    }
    
    
}
