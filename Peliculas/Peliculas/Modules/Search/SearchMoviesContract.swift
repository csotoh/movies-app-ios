//
//  SearchMoviesContract.swift
//  Peliculas
//
//  Created by Soto Rodriguez on 2/12/21.
//

import Foundation

protocol SearchMoviesViewContract{
    func showMessage(message: String)
    func updateListMovies(list: [SearchMovieEntity])
}

protocol SearchMoviesPresenterContract {
    func searchMovie(query: String)
}

protocol SearchMoviesInteractorContract {
    func searchMovies(query: String)
    func setCallback(callback: SearchMoviesCallbackContract)
}

protocol SearchMoviesCallbackContract {
    func error(failure: Failure)
    func listMovies(list: [SearchMovieEntity])
}
