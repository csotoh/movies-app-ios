//
//  SearchMovieEntity.swift
//  Peliculas
//
//  Created by Soto Rodriguez on 2/12/21.
//

import Foundation

struct SearchMovieEntity {
    
    var id: Int = 0
    var title: String = ""
    var originalTitle: String = ""
    var posterPath: String = ""
    
    init(id: Int, title: String, posterPath: String, originalTitle: String){
        self.title = title
        self.id = id
        self.originalTitle = originalTitle
        self.posterPath = posterPath == "" ? "http://quimicaactiva.com/wp-content/uploads/2017/08/no-disponible.png" : "https://image.tmdb.org/t/p/w500" + posterPath
    }
}
