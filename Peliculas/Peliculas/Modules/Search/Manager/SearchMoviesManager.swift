//
//  SearchMoviesManager.swift
//  Peliculas
//
//  Created by Soto Rodriguez on 2/12/21.
//

import Foundation

class SearchMoviesManager{
    
    var objects: [DetailMovieApiModel] = []
    
    func getSearchMovies(query: String, completion: @escaping ([DetailMovieApiModel], Failure?) -> ()) {
        
        MoviesAPI.shared.doGetServices(service: MoviesAPI.Service.search, parameters: ParameterGetSearch(query: query))
        MoviesAPI.shared.completionHandler{ (data, failure) in
            guard failure == nil else {
                completion([], failure)
                return
            }
            guard let response = data as? MovieApiModel else{
                completion([], Failure(.read))
                return
            }
            completion(response.results, nil)
        }
        
    }
}
