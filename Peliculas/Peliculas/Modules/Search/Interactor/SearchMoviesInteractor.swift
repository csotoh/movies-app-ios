//
//  SearchMoviesInteractor.swift
//  Peliculas
//
//  Created by Soto Rodriguez on 2/12/21.
//

import Foundation

class SearchMoviesInteractor: SearchMoviesInteractorContract {
    
    var callback: SearchMoviesCallbackContract?
    var manager = SearchMoviesManager()
    var listMovies: [SearchMovieEntity] = []
    
    func searchMovies(query: String) {
        self.manager.getSearchMovies(query: query, completion: { (list, failure) in
            guard failure == nil else{
                self.callback?.error(failure: failure!)
                return
            }
            self.listMovies.removeAll()
            self.listMovies.append(contentsOf: MoviesMapper.parse(from: list))
            self.callback?.listMovies(list: self.listMovies)
        })
    }
    
    func setCallback(callback: SearchMoviesCallbackContract) {
        self.callback = callback
    }
    
    
}
