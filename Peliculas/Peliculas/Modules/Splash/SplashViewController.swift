//
//  ViewController.swift
//  Peliculas
//
//  Created by Soto Rodriguez on 30/11/21.
//

import UIKit

class SplashViewController: UIViewController {

    @IBOutlet weak var imgLogo: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.animation()
    }
    
    func animation(){
        UIView.animate(withDuration: 2.0, animations: {() -> Void in
            self.imgLogo.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }, completion: {(_ finished: Bool) -> Void in
            UIView.animate(withDuration: 1.0, animations: {() -> Void in
                self.imgLogo.transform = CGAffineTransform(scaleX: 1, y: 1)
                let mainQueue = DispatchQueue.main
                let deadline = DispatchTime.now() + .seconds(3)
                mainQueue.asyncAfter(deadline: deadline) {
                    self.goPopularMovies()
                }
            })
        })
    }

    func goPopularMovies(){
        
        let vc = ListMoviesViewController(nibName: "ListMoviesViewController", bundle: nil)
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true)
    }

}

