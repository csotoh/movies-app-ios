//
//  ListMoviesInteractor.swift
//  Peliculas
//
//  Created by Soto Rodriguez on 30/11/21.
//

import Foundation

class ListMoviesInteractor : ListMoviesInteractorContract{
    
    var callback: ListMoviesCallbackContract?
    var manager = ListMoviesManager()
    var listMovies: [MovieEntity] = []
    
    func getPageMovies(page: Int16) {
        self.manager.getPopularMovies(page: page, completion: { (list, failure) in
            guard failure == nil else{
                self.callback?.error(failure: failure!)
                return
            }
            self.listMovies.append(contentsOf: MoviesMapper.parse(from: list))
            self.callback?.listMovies(list: self.listMovies)
        })
    }
    
    func setCallback(callback: ListMoviesCallbackContract) {
        self.callback = callback
    }
    
    
}
