//
//  ListMoviesContract.swift
//  Peliculas
//
//  Created by Soto Rodriguez on 30/11/21.
//

import Foundation

protocol ListMoviesViewContract{
    func showMessage(message: String)
    func updateListMovies(list: [MovieEntity])
    func noConnection()
    func presentLoading()
}

protocol ListMoviesPresenterContract {
    func selectMovie(id: Int)
    func getNextPage()
}

protocol ListMoviesInteractorContract {
    func getPageMovies(page: Int16)
    func setCallback(callback: ListMoviesCallbackContract)
}

protocol ListMoviesCallbackContract {
    func error(failure: Failure)
    func listMovies(list: [MovieEntity])
}
