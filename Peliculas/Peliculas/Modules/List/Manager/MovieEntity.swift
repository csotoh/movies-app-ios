//
//  MovieEntity.swift
//  Peliculas
//
//  Created by Soto Rodriguez on 1/12/21.
//

import Foundation

struct MovieEntity {
    
    var id: Int = 0
    var title: String = ""
    var posterPath: String = ""
    
    init(id: Int, title: String, posterPath: String){
        self.title = title
        self.id = id
        self.posterPath = posterPath == "" ? "http://www.musicapopular.cult.cu/wp-content/uploads/2017/12/imagen-no-disponible.png" : "https://image.tmdb.org/t/p/w500" + posterPath
    }
}
