//
//  ListMoviesManager.swift
//  Peliculas
//
//  Created by Soto Rodriguez on 1/12/21.
//

import Foundation

class ListMoviesManager{
    
    var objects: [DetailMovieApiModel] = []
    
    func getPopularMovies(page: Int16, completion: @escaping ([DetailMovieApiModel], Failure?) -> ()) {
        
        MoviesAPI.shared.doGetServices(service: MoviesAPI.Service.popular, parameters: ParameterGetPopular(page: page))
        MoviesAPI.shared.completionHandler{ (data, failure) in
            guard failure == nil else {
                completion([], failure)
                return
            }
            guard let response = data as? MovieApiModel else{
                completion([], Failure(.read))
                return
            }
            completion(response.results, nil)
        }
        
    }
}
