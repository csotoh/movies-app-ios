//
//  ListMoviesPresenter.swift
//  Peliculas
//
//  Created by Soto Rodriguez on 30/11/21.
//

import Foundation


class ListMoviesPresenter: ListMoviesPresenterContract, ListMoviesCallbackContract {
    
    
    var view: ListMoviesViewContract?
    var interactor: ListMoviesInteractorContract?
    var page: Int16 = 1
    var isLoading = false
    
    init(view: ListMoviesViewContract){
        self.view = view
        self.interactor = ListMoviesInteractor()
        self.interactor?.setCallback(callback: self)
        getNextPage()
    }
    
    //MARK: Presenter
    func selectMovie(id: Int) {
        
    }
    
    func getNextPage() {
        guard !self.isLoading else {
            return
        }
        self.view?.presentLoading()
        self.isLoading = true
        self.interactor?.getPageMovies(page: self.page)
        self.page += 1
    }
    
    
    //MARK: Callback
    func error(failure: Failure) {
        self.page -= 1
        self.isLoading = false
        if failure.code == CodeFailure.connection {
            self.view?.noConnection()
            return
        }
        self.view?.showMessage(message: failure.getMessageFailure())
    }
    
    func listMovies(list: [MovieEntity]) {
        self.isLoading = false
        self.view?.updateListMovies(list: list)
    }
    
}
