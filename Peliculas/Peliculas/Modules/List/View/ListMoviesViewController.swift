//
//  ListMoviesViewController.swift
//  Peliculas
//
//  Created by Soto Rodriguez on 30/11/21.
//

import UIKit

class ListMoviesViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, ListMoviesViewContract {
    
    @IBOutlet weak var stack_loading: UIStackView!
    @IBOutlet weak var stackNoConnection: UIStackView!
    @IBOutlet weak var collectionMovies: UICollectionView!
    @IBOutlet weak var textNoInternet: UILabel!
    
    let cellIdentifier = "movieCell"
    var presenter: ListMoviesPresenterContract?
    var listMovies: [MovieEntity] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        self.collectionMovies.register(UINib(nibName: "MovieViewCell", bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
        self.collectionMovies.delegate = self
        self.collectionMovies.dataSource = self
        
       self.presenter = ListMoviesPresenter(view: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        
        let statusBar = UIView(frame: window?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        statusBar.backgroundColor = .white
         window?.addSubview(statusBar)
    }

    //MARK: View
    func showMessage(message: String) {
        self.stack_loading.isHidden = true
        self.stackNoConnection.isHidden = true
        Utilities.showAlertWithOneCallback("Películas", msg: message, controller: self, callBackOk: {
            self.presenter?.getNextPage()
        })
    }
    
    func updateListMovies(list: [MovieEntity]) {
        self.listMovies = list
        self.collectionMovies.isHidden = false
        self.stack_loading.isHidden = true
        self.collectionMovies.reloadData()
    }
    
    func presentLoading() {
        self.stackNoConnection.isHidden = true
        self.stack_loading.isHidden = false
    }
    
    func noConnection() {
        self.stack_loading.isHidden = true
        self.stackNoConnection.isHidden = false
    }
    
    //MARK: Collection View
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.listMovies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionMovies.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! MovieViewCell
        
        cell.movie = self.listMovies[indexPath.row]
        cell.setUp()
        cell.dropShadow()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row  >= self.listMovies.count - 3 {
            self.presenter?.getNextPage()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let id = self.listMovies[indexPath.row].id
        let vc = DetailMovieViewController(nibName: "DetailMovieViewController", bundle: nil)
        vc.id = id
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true)
    }
    
    //MARK: Actions
    
    @IBAction func retry(_ sender: UIButton) {
        self.presenter?.getNextPage()
    }
    
    @IBAction func search(_ sender: UIBarButtonItem) {
        let vc = SearchMoviesViewController(nibName: "SearchMoviesViewController", bundle: nil)
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true)
    }
}

extension ListMoviesViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let widthLay = self.view.frame.width/2 - 18
        return CGSize(width: widthLay, height: widthLay * 1.5)
    }
}

extension UIApplication {
   var statusBarView: UIView? {
      if responds(to: Selector(("statusBar"))) {
         return value(forKey: "statusBar") as? UIView
      }
      return nil
   }
}



