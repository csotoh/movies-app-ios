//
//  MovieViewCell.swift
//  Peliculas
//
//  Created by Soto Rodriguez on 1/12/21.
//

import UIKit

class MovieViewCell: UICollectionViewCell {

    @IBOutlet weak var imagePoster: UIImageView!
    
    var movie: MovieEntity?
    
    func setUp() {
        guard let movie = self.movie else { return }
        imagePoster.downloaded(from: movie.posterPath)
        imagePoster.layer.cornerRadius = 10
        imagePoster.clipsToBounds = true
    }
    

}
