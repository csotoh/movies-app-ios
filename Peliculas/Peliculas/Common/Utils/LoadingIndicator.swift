//
//  LoadingIndicator.swift
//  Peliculas
//
//  Created by Soto Rodriguez on 1/12/21.
//

import Foundation
import UIKit

class LoadingIndicator {
    
    static var loadingActivityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        
        indicator.style = .large
        indicator.color = .white
            
        indicator.startAnimating()
        indicator.autoresizingMask = [
            .flexibleLeftMargin, .flexibleRightMargin,
            .flexibleTopMargin, .flexibleBottomMargin
        ]
            
        return indicator
    }()
    
    static var blurEffectView: UIVisualEffectView = {
        let blurEffect = UIBlurEffect(style: .dark)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            
            blurEffectView.alpha = 0.5
            blurEffectView.autoresizingMask = [
                .flexibleWidth, .flexibleHeight
            ]
            
            return blurEffectView
    }()
    
    static func show(controller: UIViewController) {
        
        let view = controller.view!
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        blurEffectView.frame = view.bounds
        view.insertSubview(blurEffectView, at: 0)
        loadingActivityIndicator.center = CGPoint(
            x: view.bounds.midX,
            y: view.bounds.midY
        )
        view.addSubview(loadingActivityIndicator)
    }
}
