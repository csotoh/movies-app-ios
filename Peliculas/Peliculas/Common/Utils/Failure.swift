//
//  Failure.swift
//  Peliculas
//
//  Created by Soto Rodriguez on 1/12/21.
//

import Foundation

enum CodeFailure {
    case server, connection, read
}

class Failure {
    
    var code: CodeFailure
    
    init(_ code: CodeFailure) {
        self.code = code
        
    }
    
    func getMessageFailure() -> String {
        switch self.code {
        case .server:
            return "Se presento un problema con el servicio, por favor intentelo de nuevo."
        case .connection:
            return "Se ha detectado un error en tu conexión a internet, compruebela e intentelo nuevamente"
        default:
            return "Se ha presentado un error en los datos"
        }
    }
}
