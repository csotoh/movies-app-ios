//
//  Utilities.swift
//  Peliculas
//
//  Created by Soto Rodriguez on 1/12/21.
//

import Foundation
import UIKit

class Utilities: NSObject {
    
    static func  showAlertWithOneCallback(_ title: String, msg: String, controller: UIViewController, callBackOk:@escaping ()->Void){
        
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        
        let accept = UIAlertAction(title: "Reintentar", style: .default, handler: {action in
            callBackOk()
        })
        
        alertController.addAction(accept)
        controller.present(alertController, animated: true, completion: nil)
        
    }
}
