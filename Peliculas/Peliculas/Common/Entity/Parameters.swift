//
//  Parameters.swift
//  Peliculas
//
//  Created by Soto Rodriguez on 1/12/21.
//

import Foundation

struct ParameterGetPopular {
    
    var page: Int16
    
    init(page: Int16){
        self.page = page
    }
    
    func getParameters() -> [String: Any]{
        return [
            "api_key"  : "ef89fddef63f897a3bcbc49fc98c3079",
            "page"     : self.page,
            "language" : "es-ES"
        ]
    }
}

struct ParameterGetDetail {
    
    var id: Int
    
    init(id: Int){
        self.id = id
    }
    
    func getParameters() -> [String: Any]{
        return [
            "api_key"  : "ef89fddef63f897a3bcbc49fc98c3079",
            "language" : "es-ES"
        ]
    }
}

struct ParameterGetSearch {
    
    var query: String
    
    init(query: String){
        self.query = query
    }
    
    func getParameters() -> [String: Any]{
        return [
            "api_key"  : "ef89fddef63f897a3bcbc49fc98c3079",
            "language" : "es-ES",
            "query"    : query
        ]
    }
}
