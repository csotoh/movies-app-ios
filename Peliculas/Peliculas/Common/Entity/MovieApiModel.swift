//
//  MoviesModel.swift
//  Peliculas
//
//  Created by Soto Rodriguez on 30/11/21.
//

import Foundation

// MARK: - PopularModel
struct MovieApiModel: Codable {
    let page: Int
    let results: [DetailMovieApiModel]
    let totalPages, totalResults: Int

    enum CodingKeys: String, CodingKey {
        case page, results
        case totalPages = "total_pages"
        case totalResults = "total_results"
    }
}




