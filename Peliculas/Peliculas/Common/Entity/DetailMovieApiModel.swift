//
//  DetailMovieApiModel.swift
//  Peliculas
//
//  Created by Soto Rodriguez on 1/12/21.
//

import Foundation


struct DetailMovieApiModel: Codable {
    let adult: Bool
    let backdropPath: String?
    let id: Int
    let originalLanguage: String
    let originalTitle, overview: String
    let posterPath : String?
    let title: String
    let voteAverage: Double

    enum CodingKeys: String, CodingKey {
        case adult
        case backdropPath = "backdrop_path"
        case id
        case originalLanguage = "original_language"
        case originalTitle = "original_title"
        case overview
        case posterPath = "poster_path"
        case title
        case voteAverage = "vote_average"
    }
}
