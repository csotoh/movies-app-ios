//
//  MoviesAPI.swift
//  Peliculas
//
//  Created by Soto Rodriguez on 1/12/21.
//

import Foundation
import Alamofire

class MoviesAPI: SessionDelegate {
    
    enum Service {
        case popular
        case search
        case detail
    }
    
    //MARK: EndPoints
    let POPULAR = "/movie/popular"
    let SEARCH = "/search/movie"
    let MOVIE = "/movie"
    
    //MARK: Variables
    static var shared: MoviesAPI = MoviesAPI()
    typealias callback<T> = (_ data: T?, _ status: Failure?) -> Void
    var callbackResponse: callback<Any>?
    let urlBase = "https://api.themoviedb.org/3"
    var alamofireManager : Session?
    
    private init(){
        super.init()
        let configuration = URLSessionConfiguration.ephemeral
        configuration.timeoutIntervalForRequest = 100
        
        alamofireManager = Session.init(configuration: configuration)
    }
    
    //MARK: Consumo Metodos GET
    func doGetServices<T>(service: Service, parameters: T? = nil) -> Void{
        
        var urlRequest = urlBase
        var parameterRequest: Parameters = [:]
        let headers: HTTPHeaders = [
            "Content-Type": "application/json"
        ]
        
        switch service {
        case .popular:
            guard let parameter = parameters as? ParameterGetPopular ?? nil else{
                self.callbackResponse?(nil, Failure(.read))
                return
            }
            urlRequest += POPULAR
            parameterRequest = parameter.getParameters()
            
        case .search:
            guard let parameter = parameters as? ParameterGetSearch ?? nil else{
                self.callbackResponse?(nil, Failure(.read))
                return
            }
            urlRequest += SEARCH
            parameterRequest = parameter.getParameters()
            
        default:
            guard let parameter = parameters as? ParameterGetDetail ?? nil else{
                self.callbackResponse?(nil, Failure(.read))
                return
            }
            urlRequest += MOVIE + "/\(parameter.id)"
            parameterRequest = parameter.getParameters()
        }
        
        self.alamofireManager!.request(urlRequest, method: .get, parameters: parameterRequest, headers: headers).response {
            (responseData) in switch responseData.result {
                
            case .success(let data):
                let codeStatus = responseData.response?.statusCode ?? 0
                switch responseData.response?.statusCode {
                case 200:
                    self.responseData(data: data, service: service, codeStatus: codeStatus)
                default:
                    self.callbackResponse?(nil, Failure(.server))
                }
                
            case .failure(_):
                self.callbackResponse?(nil, Failure(.connection))
            }
        }
    }
    
    //MARK: Procesamiento del Response
    func responseData(data: Data?, service: Service, codeStatus: Int){
        
        switch service {
        case .detail:
            do {
                let response = try JSONDecoder().decode(DetailMovieApiModel.self, from: data!)
                self.callbackResponse?(response, nil)
            } catch  {
                self.callbackResponse?(nil, Failure(.read))
            }
            
        default:
            do {
                let response = try JSONDecoder().decode(MovieApiModel.self, from: data!)
                self.callbackResponse?(response, nil)
            } catch (let error)  {
                self.callbackResponse?(nil, Failure(.read))
            }
            
            
        }
    }
    
    func completionHandler(callback: @escaping callback<Any>) {
        self.callbackResponse = callback
    }
}
