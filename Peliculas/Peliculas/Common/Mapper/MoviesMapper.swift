//
//  MoviesMapper.swift
//  Peliculas
//
//  Created by Soto Rodriguez on 1/12/21.
//

import Foundation

class MoviesMapper {
    
    static func parse(from movies: [DetailMovieApiModel]) -> [MovieEntity] {
        
        let arrayMovies = movies.map { MovieEntity(id: $0.id, title: $0.title, posterPath: $0.posterPath ?? "")}
        return arrayMovies
    }
    
    static func parse(from movie: DetailMovieApiModel) -> DetailMovieEntity {
        
        return DetailMovieEntity(id: movie.id, title: movie.title, backdropPath: movie.backdropPath ?? "", originalTitle: movie.originalTitle, overview: movie.overview, voteAverage: movie.voteAverage, posterPath: movie.posterPath ?? "")
    }
    
    static func parse(from movies: [DetailMovieApiModel]) -> [SearchMovieEntity] {
        
        let arrayMovies = movies.map { SearchMovieEntity(id: $0.id, title: $0.title, posterPath: $0.posterPath ?? "", originalTitle: $0.originalTitle)}
        return arrayMovies
    }
}
